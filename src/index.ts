import {ProjectConfig} from "@src/types/project-config";
import {configureProject} from "@src/gitlab/gitlab-api";
import config from "config";

(async function() {
    // Extract params from env vars
    const projectPath: string | undefined = process.env["PROJECT_PATH"];

    const token: string | undefined = process.env["GITLAB_TOKEN"];
    const oauthToken: string | undefined = process.env["OAUTH_TOKEN"];
    const jobToken: string | undefined = process.env["CI_TOKEN"];

    // Verify params exist
    if (!projectPath) {
        console.error("Missing env var: 'PROJECT_PATH'");
        process.exit(1)
    }
    if (!token && !oauthToken && !jobToken) {
        console.error("One of 'GITLAB_TOKEN', 'OAUTH_TOKEN', or 'CI_TOKEN' must be provided.")
        process.exit(1)
    }

    // Acquire configuration for project type
    console.log(`Config type set to: ${process.env["NODE_ENV"]}`);
    const projectConfig: ProjectConfig = <ProjectConfig>config.util.toObject();

    // Set GitBreaker api config
    const opts = {
        host: process.env["GITLAB_HOST"] ?? "https://gitlab.com",
        token,
        oauthToken,
        jobToken
    }

    // Call gitlab apis to configure the project repository
    await configureProject(projectPath, projectConfig, opts);

    process.exit(0);
})()
