import {Types} from "@gitbeaker/node";
import {GitLabApi} from "@src/types/gitbreaker-api";
import {ProtectedBranch} from "@src/types/project-config";


export async function protectBranches(api: GitLabApi, projectId: number, protectedBranches: ProtectedBranch[]): Promise<void> {
    console.log("Protecting branches...")

    // Pull existing branches
    const existingProtectedBranches: Types.ProtectedBranchSchema[] = await api.ProtectedBranches.all(projectId);

    // Handle each branch
    for (const branch of protectedBranches) {
        const existingBranch: Types.ProtectedBranchSchema | undefined = existingProtectedBranches.find(exBr => exBr.name === branch.name);

        // Check if the branch is already protected.
        if (existingBranch) {
            // If the existing branch has different settings, un-protect it so we can re-add it with correct settings later.
            if (!compareBranchSettings(existingBranch, branch)) {
                console.log(`Unprotecting '${branch.name}' since it has different settings...`)
                await api.ProtectedBranches.unprotect(projectId, branch.name)
            } else { // Otherwise continue to the next branch
                console.log(`Branch '${branch.name}' is already protected with correct settings.`)
                continue;
            }
        }

        // Protect (or re-protect) the branch.
        await api.ProtectedBranches.protect(projectId, branch.name, mapProtectedBranchOptions(branch))
        console.log(`Protected branch '${branch.name}].`)
    }
}

// Map our UI-convenient config names to their api equivalents.
function mapProtectedBranchOptions(branch: ProtectedBranch): Record<string, unknown> {
    return {
        push_access_level: branch.pushAccessLevel,
        merge_access_level: branch.mergeAccessLevel,
        unprotect_access_level: branch.unprotectAccessLevel,
        allow_force_push: branch.allowForcePush,
        code_owner_approval_required: branch.codeOwnerApprovalRequired
    }
}

// Compare an existing branch's settings against a to-be-protected branch's settings.
// Note: Arrays of access levels it not currently supported.
function compareBranchSettings(existingBranch: Types.ProtectedBranchSchema, newBranch: ProtectedBranch): boolean {
    return newBranch.allowForcePush === existingBranch.allow_force_push &&
        newBranch.codeOwnerApprovalRequired === existingBranch.code_owner_approval_required &&
        newBranch.mergeAccessLevel === existingBranch.merge_access_levels?.[0].access_level &&
        newBranch.pushAccessLevel === existingBranch.push_access_levels?.[0].access_level
        //newBranch.unprotectAccessLevel === existingBranch.unprotect_access_level?.[0].access_level    Gitbreaker doesn't support this value yet.
}
