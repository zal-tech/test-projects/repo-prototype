import {Gitlab, Types} from '@gitbeaker/node';
import {applyGeneralSettings} from "@src/gitlab/general-settings";
import {protectBranches} from "@src/gitlab/branches";
import {protectTags} from "@src/gitlab/tags";
import {GitLabApi} from "@src/types/gitbreaker-api";
import {Environment, ProjectConfig, ProtectedBranch, ProtectedTag} from "@src/types/project-config";
import {configureEnvironments} from "@src/gitlab/environments";

export async function configureProject(projectPath: string, projectConfig: ProjectConfig, gitBreakerConfig: Record<string, string | undefined>): Promise<void> {
    const api: GitLabApi = new Gitlab(gitBreakerConfig);
    const projectName: string = projectPath.split("/").slice(-1)[0]

    console.log("Looking up project...")
    const search: Types.ProjectSchema[] = await api.Projects.search(projectName)
    const project: Types.ProjectSchema | undefined = search.find(s => s.path_with_namespace === projectPath)

    if (!project) {
        console.error(`Unable to find project '${projectPath}'.`);
        process.exit(1)
    }

    // Project Details
    const projectId: number = project.id;

    // Set General Settings
    try {
        // Apply General settings
        await applyGeneralSettings(api, projectId, projectConfig);

        // Protect Branches
        const protectedBranches: ProtectedBranch[] = projectConfig.repository.protectedBranches ?? [];
        if (protectedBranches.length > 0) {
            await protectBranches(api, projectId, protectedBranches);
        }

        // Protect Tags
        const protectedTags: ProtectedTag[] = projectConfig.repository.protectedTags ?? [];
        if (protectedTags.length > 0) {
            await protectTags(api, projectId, protectedTags)
        }

        // Create and Protect Environments
        const environments: Environment[] = projectConfig.cicd.environments ?? [];
        if (environments.length > 0) {
            await configureEnvironments(api, projectId, environments)
        }


    } catch (err) {
        console.error(err)
        console.error("Error occurred while trying to apply settings to repository.")
        process.exit(1)
    }

    console.log(`Settings have been applied to '${projectPath}'.`)
}
