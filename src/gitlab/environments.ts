import {Types} from "@gitbeaker/node";
import {GitLabApi} from "@src/types/gitbreaker-api";
import {Environment, EnvironmentAccessLevel} from "@src/types/project-config";

export async function configureEnvironments(api: GitLabApi, projectId: number, environments: Environment[]): Promise<void> {
    console.log("Creating and Protecting environments...")

    // Pull existing environments.
    const existingEnvironments: Types.EnvironmentSchema[] = <Types.EnvironmentSchema[]>(await api.Environments.all(projectId));

    // Handle each environment.
    for (const environment of environments) {
        const existingEnv: Types.EnvironmentSchema | undefined = existingEnvironments.find(exEnv => exEnv.name === environment.name);

        // Check if the environment exists.
        if (existingEnv) {
            // If the existing environment has different settings, edit it.
            if (!compareEnvironmentSettings(existingEnv, environment)) {
                console.log(`Updating environment '${existingEnv.name}'.`);
                await api.Environments.edit(projectId, existingEnv.id, {
                    external_url: environment.externalURL
                })
            } else { // Otherwise continue.
                console.log(`Environment '${environment.name}' already exists with correct settings.`);
            }
        } else { // If the environment doesn't exist, create it.
            console.log(`Environment '${environment.name}' has been created.`);
            await api.Environments.create(projectId, {
                name: environment.name,
                external_url: environment.externalURL
            })
        }

        // Handle environment protection.
        // ToDo: Add support for comparing a protected environment's access level (Gitbreaker doesn't support protected environments yet).
        // https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/api/protected_environments.md
    }
}

// Compare an existing environment's settings against a to-be-edited/protected environment's settings.
function compareEnvironmentSettings(existingEnvironment: Types.EnvironmentSchema, newEnv: Environment): boolean {
    return newEnv.externalURL === existingEnvironment.external_url
}
