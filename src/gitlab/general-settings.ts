import {GitLabApi} from "@src/types/gitbreaker-api";
import {ProjectConfig} from "@src/types/project-config";

export async function applyGeneralSettings(api: GitLabApi, projectId: number, projectConfig: ProjectConfig): Promise<void> {
    console.log("Applying general repository settings...")
    await api.Projects.edit(projectId, mapGeneralOptions(projectConfig))
    console.log("General settings updated.")
}

// Map our UI-convenient config names to their api equivalents.
function mapGeneralOptions(config: ProjectConfig): Record<string, unknown> {
    return {
        allow_merge_on_skipped_pipeline: config.mergeRequests.skippedPipelinesConsideredSuccessful,
        analytics_access_level: config.featuresAndPermissions.analytics,
        auto_cancel_pending_pipelines: config.cicd.autoCancelRedundantPipelines,
        auto_devops_enabled: config.cicd.autoDevops,
        autoclose_referenced_issues: config.repository.autoCloseReferencedIssuesOnDefaultBranch,
        build_coverage_regex: config.cicd.testCoverageParsing,
        build_git_strategy: config.cicd.gitStrategy,
        build_timeout: config.cicd.timeout,
        builds_access_level: config.featuresAndPermissions.cicd,
        ci_default_git_depth: config.cicd.gitShallowClone,
        ci_forward_deployment_enabled: config.cicd.skipOutdatedDeploymentJobs,
        container_registry_access_level: config.featuresAndPermissions.containerRegistry,
        default_branch: config.repository.defaultBranch,
        emails_disabled: config.featuresAndPermissions.disableEmailNotifications,
        forking_access_level: config.featuresAndPermissions.forks,
        issues_access_level: config.featuresAndPermissions.issues,
        lfs_enabled: config.featuresAndPermissions.gitLFS,
        merge_method: config.mergeRequests.mergeMethod,
        merge_requests_access_level: config.featuresAndPermissions.mergeRequests,
        operations_access_level: config.featuresAndPermissions.operations,
        only_allow_merge_if_all_discussions_are_resolved: config.mergeRequests.allDiscussionsMustBeResolved,
        only_allow_merge_if_pipeline_succeeds: config.mergeRequests.pipelinesMustSucceed,
        packages_enabled: config.featuresAndPermissions.packages,
        pages_access_level: config.featuresAndPermissions.pages,
        requirements_access_level: config.featuresAndPermissions.requirements,
        public_builds: config.cicd.publicPipelines,
        remove_source_branch_after_merge: config.mergeRequests.enableDeleteSourceBranchOptionByDefault,
        repository_access_level: config.featuresAndPermissions.repository,
        request_access_enabled: config.featuresAndPermissions.usersCanRequestAccess,
        resolve_outdated_diff_discussions: config.mergeRequests.autoResolveOutdatedMRDiscussions,
        service_desk_enabled: config.activateServiceDesk,
        shared_runners_enabled: config.cicd.enableSharedRunners,
        show_default_award_emojis: config.featuresAndPermissions.showAwardEmojis,
        snippets_access_level: config.featuresAndPermissions.snippets,
        squash_option: config.mergeRequests.squashCommitsWhenMerging,
        wiki_access_level: config.featuresAndPermissions.wiki,
        keep_latest_artifact: config.cicd.keepMostRecentArtifacts
    }
}
