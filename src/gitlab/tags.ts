import {Types} from "@gitbeaker/node";
import {GitLabApi} from "@src/types/gitbreaker-api";
import {ProtectedTag} from "@src/types/project-config";


export async function protectTags(api: GitLabApi, projectId: number, protectedTags: ProtectedTag[]): Promise<void> {
    console.log("Protecting tags...")

    // Pull existing tags
    const existingProtectedTags: Types.ProtectedTagSchema[] = await api.ProtectedTags.all(projectId);

    // Handle each tag
    for (const tag of protectedTags) {
        const existingTag: Types.ProtectedTagSchema | undefined = existingProtectedTags.find(exTag => exTag.name === tag.name)

        // Check if the tag is already protected.
        if (existingTag) {
            // If the existing tag has different settings, un-protect it so we can re-add it with correct settings later.
            if (!compareTagSettings(existingTag, tag)) {
                console.log(`Unprotecting '${tag.name}' since it has different settings...`)
                await api.ProtectedTags.unprotect(projectId, tag.name)
            } else { // Otherwise continue to the next tag
                console.log(`Tag '${tag.name}' is already protected with correct settings.`)
                continue;
            }
        }

        // Protect (or re-protect) the branch.
        await api.ProtectedTags.protect(projectId, tag.name, mapProtectedTagOptions(tag))
        console.log(`Protected tag '${tag.name}].`)
    }
}

// Map our UI-convenient config names to their api equivalents.
function mapProtectedTagOptions(tag: ProtectedTag): Record<string, unknown> {
    return {
        create_access_level: tag.accessLevel
    }
}

// Compare an existing tag's settings against a to-be-protected tag's settings.
function compareTagSettings(existingTag: Types.ProtectedTagSchema, newTag: ProtectedTag): boolean {
    return newTag.accessLevel === existingTag.create_access_levels?.[0].access_level
}
