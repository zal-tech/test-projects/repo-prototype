type AccessLevel = "disabled" | "private" | "enabled";

export type DeepPartial<T> = {
    [propertyKey in keyof T]?: DeepPartial<T[propertyKey]>;
};

// Options are named and ordered similar to how they appear in the Gitlab UI.
export interface ProjectConfig {
    // Visibility, features, and permissions
    featuresAndPermissions: {
        usersCanRequestAccess: boolean;
        issues: AccessLevel;
        enableCVEIDs: boolean; // ToDo: [cve_id_request_enabled] - Not supported via api yet.
        repository: AccessLevel;
        mergeRequests: AccessLevel;
        forks: AccessLevel;
        gitLFS: boolean;
        packages: boolean;
        cicd: AccessLevel;
        containerRegistry: AccessLevel;
        analytics: AccessLevel;
        requirements: AccessLevel | "public";
        securityAndCompliance: AccessLevel; //  ToDo: [security_and_compliance_access_level] - Not support via api yet.
        wiki: AccessLevel;
        snippets: AccessLevel;
        pages: AccessLevel | "public";
        operations: AccessLevel;
        metrics: "public" | "private "// ToDo: [metrics_dashboard_access_level] - Not supported via api yet.
        showAwardEmojis: boolean;
        disableEmailNotifications: boolean;
    }

    // Merge Requests
    mergeRequests: {
        mergeMethod: "merge" | "rebase_merge" | "ff"
        enableMergedResultsPipelines: boolean; // ToDo: [merge_pipelines_enabled] - Not supported via api yet
        enableMergeTrains: boolean; // ToDo: [merge_trains_enabled] - Not supported via api yet
        autoResolveOutdatedMRDiscussions: boolean;
        showMRLinkInCommandLine: boolean; // ToDo: [printing_merge_request_link_enabled] - Not supported via api yet
        enableDeleteSourceBranchOptionByDefault: boolean;
        squashCommitsWhenMerging: "never" | "always" | "default_on" | "default_off";
        pipelinesMustSucceed: boolean;
        skippedPipelinesConsideredSuccessful: boolean;
        allDiscussionsMustBeResolved: boolean;
    }

    activateServiceDesk: boolean;

    // Repository
    repository: {
        defaultBranch: string;
        autoCloseReferencedIssuesOnDefaultBranch: boolean;
        rejectUnverifiedUsers: boolean; // ToDo: [push rule: commit_committer_check] - Not supported via api yet
        rejectUnsignedCommits: boolean; // ToDo: [push rule: reject_unsigned_commits] - Not supported via api yet
        protectedBranches?: ProtectedBranch[];
        protectedTags?: ProtectedTag[];
    }

    // CI/CD
    cicd: {
        publicPipelines: boolean;
        autoCancelRedundantPipelines: "enabled" | "disabled";
        skipOutdatedDeploymentJobs: boolean;
        gitStrategy: "fetch" | "clone";
        gitShallowClone: number;
        timeout: number;
        testCoverageParsing: string;
        autoDevops: boolean;
        environments?: Environment[];
        enableSharedRunners: boolean;
        keepMostRecentArtifacts: boolean;
    }
}

export interface ProtectedBranch {
    name: string;
    pushAccessLevel: number;
    mergeAccessLevel: number;
    unprotectAccessLevel: number;
    allowForcePush: boolean;
    codeOwnerApprovalRequired: boolean;
}

export interface ProtectedTag {
    name: string;
    accessLevel: number;
}

export type EnvironmentAccessLevel = {user_id: number} | {group_id: number} | {access_level: number};

export interface Environment {
    name: string;
    externalURL?: string;
    accessLevels?: Array<EnvironmentAccessLevel>;
}
