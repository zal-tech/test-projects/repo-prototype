import {Gitlab} from '@gitbeaker/node';

export type GitLabApi = typeof Gitlab.prototype
