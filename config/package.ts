import {ProjectConfig, DeepPartial} from "@src/types/project-config";

const packageConfig: DeepPartial<ProjectConfig> = {
    featuresAndPermissions: {
        packages: true
    }
}

export default packageConfig;
