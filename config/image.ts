import {ProjectConfig, DeepPartial} from "@src/types/project-config";

const packageConfig: DeepPartial<ProjectConfig> = {
    featuresAndPermissions: {
        containerRegistry: "enabled"
    }
}

export default packageConfig;
