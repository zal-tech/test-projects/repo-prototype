import {ProjectConfig} from "@src/types/project-config";

const defaultConfig: ProjectConfig = {
    // Visibility, features, and permissions
    featuresAndPermissions: {
        usersCanRequestAccess: false,
        issues: "enabled",
        enableCVEIDs: false, // [cve_id_request_enabled] - Not supported via api yet.
        repository: "enabled",
        mergeRequests: "private",
        forks: "enabled",
        gitLFS: false,
        packages: false,
        cicd: "private",
        containerRegistry: "disabled",
        analytics: "private",
        requirements: "disabled",
        securityAndCompliance: "disabled", //  [security_and_compliance_access_level] - Not support via api yet.
        wiki: "enabled",
        snippets: "enabled",
        pages: "disabled",
        operations: "private",
        metrics: "private ", // [metrics_dashboard_access_level] - Not supported via api yet.
        showAwardEmojis: true,
        disableEmailNotifications: true,
    },
    // Merge Requests
    mergeRequests: {
        mergeMethod: "rebase_merge",
        enableMergedResultsPipelines: true, // [merge_pipelines_enabled] - Not supported via api yet
        enableMergeTrains: true, // [merge_trains_enabled] - Not supported via api yet
        autoResolveOutdatedMRDiscussions: false,
        showMRLinkInCommandLine: true, // [printing_merge_request_link_enabled] - Not supported via api yet
        enableDeleteSourceBranchOptionByDefault: true,
        squashCommitsWhenMerging: "default_on",
        pipelinesMustSucceed: true,
        skippedPipelinesConsideredSuccessful: false,
        allDiscussionsMustBeResolved: true,
    },
    activateServiceDesk: false,
    // Repository
    repository: {
        defaultBranch: "main",
        autoCloseReferencedIssuesOnDefaultBranch: true,
        rejectUnverifiedUsers: true, // [push rule: commit_committer_check] - Not supported via api yet
        rejectUnsignedCommits: true, // [push rule: reject_unsigned_commits] - Not supported via api yet
        protectedBranches: [
            {
                name: "main",
                pushAccessLevel: 40,
                mergeAccessLevel: 40,
                unprotectAccessLevel: 40,
                allowForcePush: false,
                codeOwnerApprovalRequired: false
            }
        ],
        protectedTags: [
            {
                name: "*",
                accessLevel: 40
            }
        ],
    },
    // CI/CD
    cicd: {
        publicPipelines: false,
        autoCancelRedundantPipelines: "enabled",
        skipOutdatedDeploymentJobs: true,
        gitStrategy: "fetch",
        gitShallowClone: 5,
        timeout: 3600,
        testCoverageParsing: "",
        autoDevops: false,
        keepMostRecentArtifacts: true,
        enableSharedRunners: true,
    }
}

export default defaultConfig;
