import {ProjectConfig, DeepPartial} from "@src/types/project-config";

const packageConfig: DeepPartial<ProjectConfig> = {
    cicd: {
        environments: [
            {
                name: "development",
                accessLevels: [
                    {access_level: 40}
                ]
            },
            {
                name: "qa",
                accessLevels: [
                    {access_level: 40}
                ]
            },
            {
                name: "production",
                accessLevels: [
                    {access_level: 40}
                ]
            }
        ]
    }
}

export default packageConfig;
